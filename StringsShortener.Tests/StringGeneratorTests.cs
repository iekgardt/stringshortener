﻿using System.Linq;
using NUnit.Framework;
using StringsShortener.Api.BusinessServices;

namespace StringsShortener.Tests
{
    [TestFixture]
    public class StringGeneratorTests
    {
        [Test]
        public void Generate_ReturnsCorrectLength()
        {
            var str = new StringGenerator().Generate(6);
            Assert.AreEqual(6, str.Count());
        }

        [Test]
        public void Generate_ReturnsRandomValue()
        {
            var str1 = new StringGenerator().Generate(6);
            var uniquenessCount = str1.ToList().GroupBy(p => p).Count();
            Assert.IsTrue(uniquenessCount > 3);
            var str2 = new StringGenerator().Generate(6);
            Assert.IsTrue(str1 != str2);
        }
    }
}
