﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;
using StringsShortener.Api.Domain;

namespace StringsShortener.Api.DataAccess
{
    public class MongoRepository<T> : IRepository<T> where T: IData
    {
        private readonly IMongoDb _database;
        
        public MongoRepository(IMongoDb database)
        {
            _database = database;
        }

        public T Get(ObjectId id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<T> GetAll()
        {
            return _database.GetCollection<T>().Find(p => true).ToListAsync().Result;
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> filter)
        {
            return _database.GetCollection<T>().Find(filter).ToListAsync().Result;
        }

        public void Save(Expression<Func<T, bool>> filter, T entity)
        {
            _database.GetCollection<T>().ReplaceOneAsync(filter, entity);
        }

        public void Add(T entity)
        {
            _database.GetCollection<T>().InsertOneAsync(entity);
        }

        public void Execute(ICommand command)
        {
            command.Execute();
        }
    }
}