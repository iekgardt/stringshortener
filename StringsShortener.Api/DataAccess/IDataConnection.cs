﻿namespace StringsShortener.Api.DataAccess
{
    public interface IDataConnection
    {
        string GetConnection();
    }
}
