﻿using MongoDB.Driver;
using StringsShortener.Api.Domain;

namespace StringsShortener.Api.DataAccess
{
    public class MongoIncrementCommand<T, TProperty> : ICommand where T : IData
    {
        private readonly IncrementParameter<T, TProperty> _parameter;
        private readonly IMongoDb _database;

        public MongoIncrementCommand(IMongoDb database, IncrementParameter<T, TProperty> parameter)
        {
            _database = database;
            _parameter = parameter;
        }
        
        public void Execute()
        {
            _database.GetCollection<T>().UpdateOne(_parameter.Filter, Builders<T>.Update.Inc(_parameter.Property, _parameter.PropertyValue));
        }
    }
}