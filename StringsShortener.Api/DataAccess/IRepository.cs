﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Bson;

namespace StringsShortener.Api.DataAccess
{
    public interface IRepository<T>
    {
        T Get(ObjectId id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetMany(Expression<Func<T, bool>> filter);
        void Save(Expression<Func<T, bool>> filter, T entity);
        void Add(T entity);
        void Execute(ICommand command);
    }
}
