﻿using StringsShortener.Api.Domain;

namespace StringsShortener.Api.DataAccess
{
    public class MongoCommandFactory<T> : ICommandFactory<T> where T : IData
    {
        private readonly IMongoDb _database;

        public MongoCommandFactory(IMongoDb database)
        {
            _database = database;
        }

        public ICommand GetIncrementCommand<TProperty>(IncrementParameter<T, TProperty> parameter)
        {
            return new MongoIncrementCommand<T, TProperty>(_database, parameter);
        }
    }
}