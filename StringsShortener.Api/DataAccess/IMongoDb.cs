﻿using MongoDB.Driver;
using StringsShortener.Api.Domain;

namespace StringsShortener.Api.DataAccess
{
    public interface IMongoDb
    {
        IMongoDatabase MongoDatabase { get; }

        IMongoCollection<T> GetCollection<T>() where T : IData;
    }
}