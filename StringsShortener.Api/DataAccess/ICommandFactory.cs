﻿namespace StringsShortener.Api.DataAccess
{
    public interface ICommandFactory<T>
    {
        ICommand GetIncrementCommand<TProperty>(IncrementParameter<T, TProperty> parameter);
    }
}
