﻿using MongoDB.Driver;

namespace StringsShortener.Api.DataAccess
{
    public class MongoContext : MongoClient
    {
        public MongoContext(IDataConnection connection) : base(connection.GetConnection()) { }
    }
}