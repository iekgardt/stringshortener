﻿using MongoDB.Driver;
using StringsShortener.Api.Domain;

namespace StringsShortener.Api.DataAccess
{
    public class MongoDb : IMongoDb
    {
        private readonly IMongoClient _client;
        public MongoDb(IMongoClient client)
        {
            _client = client;
        }

        public IMongoDatabase MongoDatabase
        {
            get { return _client.GetDatabase("StringsShortener"); }
        }

        public IMongoCollection<T> GetCollection<T>() where T : IData
        {
            return MongoDatabase.GetCollection<T>(string.Format("{0}s", typeof(T).Name));
        }
    }
}