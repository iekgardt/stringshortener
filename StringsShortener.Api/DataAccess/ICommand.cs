﻿namespace StringsShortener.Api.DataAccess
{
    public interface ICommand
    {
        void Execute();
    }
}
