﻿using System.Configuration;
using ConfigurationSettings = StringsShortener.Api.Configuration.ConfigurationSettings;

namespace StringsShortener.Api.DataAccess
{
    public class MongoConnection : IDataConnection
    {
        protected string DataConnection { get { return ConfigurationSettings.MongoConnection; } }
        
        public string GetConnection()
        {
            var serviceConnection = DataConnection;
            if (!string.IsNullOrEmpty(serviceConnection))
            {
               return serviceConnection;
            }
            throw new ConfigurationErrorsException("Connection isn't defined in configuration");
        }
    }
}