﻿using System;
using System.Linq.Expressions;

namespace StringsShortener.Api.DataAccess
{
    public class IncrementParameter<T, TProperty>
    {
        public Expression<Func<T, bool>> Filter { get; set; }
        public Expression<Func<T, TProperty>> Property { get; set; }
        public TProperty PropertyValue { get; set; }
        
        public IncrementParameter(Expression<Func<T, bool>> filter, Expression<Func<T, TProperty>> property, TProperty propertyValue)
        {
            Filter = filter;
            Property = property;
            PropertyValue = propertyValue;
        }
    }
}