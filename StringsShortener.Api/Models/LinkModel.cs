﻿namespace StringsShortener.Api.Models
{
    public class LinkModel
    {
        public string Url { get; set; }
        public int TransitionCount { get; set; }
    }
}