﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace StringsShortener.Api.Domain
{
    public class Link : IData
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Url { get; set; }
        public string ShortenedUrl { get; set; }
        public int TransitionCount { get; set; }
    }
}