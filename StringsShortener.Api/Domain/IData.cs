﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace StringsShortener.Api.Domain
{
    public interface IData
    {
        [BsonId]
        ObjectId Id { get; set; }
    }
}
