﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringsShortener.Api.BusinessServices
{
    public class StringGenerator
    {
        private const string Alfabet = "abcdefghijklmnopqrstuvwxyz0123456789";
        protected List<char> AlfabetList { get { return Alfabet.ToList(); } }

        public string Generate(int length)
        {
            var randomList =  new List<char>();
            var random = new Random();
            for (var i = 0; i < length; i++)
            {
                randomList.Add(AlfabetList.ElementAt(random.Next(0,AlfabetList.Count-1)));
            }
            var randomString = new StringBuilder();
            randomList.ForEach(p => randomString.Append(p));
            return randomString.ToString();
        }
    }
}