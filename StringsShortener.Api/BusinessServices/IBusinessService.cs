﻿using System.Collections.Generic;

namespace StringsShortener.Api.BusinessServices
{
    public interface IBusinessService<out T>
    {
        IEnumerable<T> FindAll();
    }
}
