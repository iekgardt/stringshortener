﻿using System;
using System.Linq;
using StringsShortener.Api.Configuration;
using StringsShortener.Api.DataAccess;
using StringsShortener.Api.Domain;

namespace StringsShortener.Api.BusinessServices
{
    public class LinkBusinessService : BaseBusinessService<Link>, ILinkBusinessService<Link>
    {
        private readonly ICommandFactory<Link> _factory;
        private static readonly Object SyncObject = new object();
        
        public LinkBusinessService(ICommandFactory<Link> factory, IRepository<Link> repository) : base(repository)
        {
            _factory = factory;
        }

        protected void IncrementTransitionCount(string url)
        {
            var parameter = new IncrementParameter<Link, int>(p => p.ShortenedUrl == url, p => p.TransitionCount, 1);
            _factory.GetIncrementCommand(parameter).Execute();
        }

        public Link Shorten(string url)
        {
            lock (SyncObject)
            {
                var attemptsCount = 0;
                while (attemptsCount < ConfigurationSettings.GenerationAttemptsCount)
                {
                    var randomString = new StringGenerator().Generate(6);
                    var shortenedLink = string.Format("{0}{1}", ConfigurationSettings.BaseUrl, randomString);
                    var duplicatedLink = Repository.GetMany(p => p.ShortenedUrl == shortenedLink).FirstOrDefault();
                    if (duplicatedLink == null)
                    {
                        var link = new Link {Url = url, ShortenedUrl = shortenedLink, TransitionCount = 0};
                        Repository.Add(link);
                        return link;
                    }
                    attemptsCount++;
                }
            }
            return null;
        }

        public Link GetFullLinkWithIncrement(string url)
        {
            var link = Repository.GetMany(p => p.ShortenedUrl == url).FirstOrDefault();
            if (link != null)
            {
                IncrementTransitionCount(url);
                return link;
            }
            return null;
        }
    }
}