﻿using System.Collections.Generic;
using StringsShortener.Api.DataAccess;

namespace StringsShortener.Api.BusinessServices
{
    public class BaseBusinessService<T> : IBusinessService<T>
    {
        protected IRepository<T> Repository { get; set; }

        public BaseBusinessService(IRepository<T> repository)
        {
            Repository = repository;
        }

        public IEnumerable<T> FindAll()
        {
            return Repository.GetAll();
        }
    }
}