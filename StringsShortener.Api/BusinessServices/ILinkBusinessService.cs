﻿using StringsShortener.Api.Domain;

namespace StringsShortener.Api.BusinessServices
{
    public interface ILinkBusinessService<out T> : IBusinessService<T>
    {
        Link Shorten(string url);
        Link GetFullLinkWithIncrement(string url);
    }
}
