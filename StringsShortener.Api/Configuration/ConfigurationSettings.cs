﻿using System.Configuration;

namespace StringsShortener.Api.Configuration
{
    public static class ConfigurationSettings
    {
        public static string BaseUrl
        {
             get { return ConfigurationManager.AppSettings["BaseUrl"]; }
        }

        public static string MongoConnection
        {
            get { return ConfigurationManager.AppSettings["MongoConnection"]; }
        }

        public static int GenerationAttemptsCount
        {
            get
            {
                int intValue;
                var setting = ConfigurationManager.AppSettings["GenerationAttemptsCount"];
                return setting != null && int.TryParse(setting, out intValue) ? intValue : 3;
            }
        }
    }
}