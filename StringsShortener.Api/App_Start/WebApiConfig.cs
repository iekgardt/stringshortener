﻿using System.ComponentModel;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Microsoft.Practices.ObjectBuilder2;
using StringsShortener.Api.Configuration;

namespace StringsShortener.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
