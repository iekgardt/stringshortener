using Microsoft.Practices.Unity;
using System.Web.Http;
using MongoDB.Driver;
using StringsShortener.Api.BusinessServices;
using StringsShortener.Api.DataAccess;
using StringsShortener.Api.Domain;
using Unity.WebApi;

namespace StringsShortener.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IDataConnection, MongoConnection>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMongoClient, MongoContext>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMongoDb, MongoDb>(new ContainerControlledLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(MongoRepository<>));
            container.RegisterType(typeof(ICommandFactory<>), typeof(MongoCommandFactory<>));
            container.RegisterType<ILinkBusinessService<Link>, LinkBusinessService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}