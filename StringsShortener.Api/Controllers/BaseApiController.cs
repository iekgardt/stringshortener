﻿using System.Web.Http;
using StringsShortener.Api.BusinessServices;

namespace StringsShortener.Api.Controllers
{
    public class BaseApiController<T> : ApiController
    {
        protected IBusinessService<T> BusinessService { get; set; } 
        
        public BaseApiController(IBusinessService<T> businessService)
        {
            BusinessService = businessService;
        } 
    }
}