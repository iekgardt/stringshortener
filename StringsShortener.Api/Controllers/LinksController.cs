﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using StringsShortener.Api.BusinessServices;
using StringsShortener.Api.Domain;
using StringsShortener.Api.Models;

namespace StringsShortener.Api.Controllers
{
    public class LinksController : BaseApiController<Link>
    {
        private readonly ILinkBusinessService<Link> _businessService;

        public LinksController(ILinkBusinessService<Link> businessService) : base(businessService)
        {
            _businessService = businessService;
        }

        public IEnumerable<LinkModel> Get()
        {
            var links = _businessService.FindAll();
            return links.Select(p=> new LinkModel{TransitionCount = p.TransitionCount, Url = p.ShortenedUrl});
        }

        public string Get(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                var fullLink = _businessService.GetFullLinkWithIncrement(url);
                if (fullLink != null && !string.IsNullOrEmpty(fullLink.Url))
                {
                    return fullLink.Url;
                }
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);    
        }

        
        public string Post([FromBody]string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                var newLink = _businessService.Shorten(url);
                if (newLink != null && !string.IsNullOrEmpty(newLink.ShortenedUrl))
                {
                    return newLink.ShortenedUrl;
                }
                throw new HttpResponseException(HttpStatusCode.InternalServerError);    
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);    
        }
    }
}
